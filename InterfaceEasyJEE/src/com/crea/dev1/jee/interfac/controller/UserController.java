package com.crea.dev1.jee.interfac.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.crea.dev1.jee.model.beans.User;
import com.crea.dev1.jee.model.dao.UserDAO;

/**
 * Servlet implementation class UserController
 */
@WebServlet("/UserController")
public class UserController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserController() {
        super();
        // TODO Auto-generated constructor stub
    }
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
            // ICI VIENDRA LE TRAITEMENT DE NOS REQUETES
    		System.out.println("On passe par le controller");
    		
    		// Récupération de le l'action et sa valeur associé via l'url
    		String[] actionRequest = this.getActionRequest(request);
    		String action = actionRequest[0];
    		String actionValue = actionRequest[1];
    		
    		// Initialisation du forward
    		String forward = null;
    		
    		// Répartition des actions
    		switch(action) {
    			case("index"):
    				ArrayList<User> users = UserDAO.getAll();
    				request.setAttribute("users", users);
    				forward = "views/users/index.jsp";
    				break;
    			case("show"):
    				forward = "views/users/show.jsp";
    				break;
    			case("add"):
    				forward = "views/users/add.jsp";
    				break;
    			case("edit"):
    				forward = "views/users/edit.jsp";
    				break;
    			default:
    				forward = "404.jsp";
    				break;
    		}
    		
    		// Affichage de la jsp correspondante à l'action demandée avec RequestDispatcher
    		RequestDispatcher dispatcher = request.getRequestDispatcher("/"+forward);
    		dispatcher.forward(request, response);

    }
    
    /**
     * Méthode permettant de récupérer l'action et la valeur associé à cette action de l'url.
  
     * Par exemple: Pour http://localhost:8080/InterfaceEasyJEE/users/edit/AGUE001
     * nous obtiendront un tableau avec à l'index 0 "edit" et a l'index 1 "AGUE001"
     * 
     * @param request
     * @return String[]: [0]: action [1]: actionValue
     */
    protected String[] getActionRequest(HttpServletRequest request) {
     	
    	// Définition de l'action par défault et des autre variables
    	String action = "index";
    	String actionValue = null;
    	String[] returnValue = new String[2];
    	
    	// Récupération de l'url du projet
    	// request.getRequestURI() retournera l'url complete p.ex(localhost:8080/InterfaceEasyJEE/user/add)
    	// request.getContextPath() retournera l'url du point d'entrée (p.ex: localhost:8080/InterfaceEasyJEE/)
    	// Nous allons supprimer l'url du point d'entrée de notre url complete
    	// Nous aurons ainsi uniquement le "path" 
    	// Le path est la partie de l'url qui nous intéresse (p.ex: users/add/ )
		String path = request.getRequestURI().substring(request.getContextPath().length() + 1);
		System.out.println("Path: " + path);

		// Convertion du path (p.ex /users/edit/AGUE001) en tableau de string
		String[] pathParts = path.split("/");
		Integer nbPathParts = pathParts.length; 
		
		System.out.println("pathParts: " + Arrays.toString(pathParts));
		System.out.println("nbPathParts: " + nbPathParts);

		
		if(nbPathParts == 2) {
			//p.ex:  /users/add (/1/2)
			action = pathParts[1];
		}
		if(nbPathParts == 3 ) {
			// /users/edit/AGUE001
			// /1/2/3
			action = pathParts[1];
			actionValue = pathParts[2];
		}
		
		returnValue[0] = action;
		returnValue[1] = actionValue;

		System.out.println("action: " + action);		
		System.out.println("actionValue: " + actionValue);
		
		return returnValue;
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
