<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>  
<%@page import="java.util.ArrayList"%>
<%@page import="com.crea.dev1.jee.model.beans.User"%>   
<% ArrayList<User> users = (ArrayList<User>) request.getAttribute("users"); %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste des utilisateurs</title>
</head>
<body>
	
	<a href="add">Ajouter un utilisateur</a>
	<ul>
	<% for(User user:users){ %> 
		<li>
			<%=user.getId() %> - 
			<%=user.getUsername() %> - 
			<%=user.getEmail() %> |  
			<a href="show/<%=user.getId() %>">Afficher</a> -
			<a href="edit/<%=user.getId() %>">Modifier</a>
		</li>
	<% } %>
	</ul>
	
</body>
</html>