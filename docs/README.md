- [1. Prequis](#1-prequis)
- [2. Base de données](#2-base-de-donn%C3%A9es)
- [3. Model](#3-model)
  - [3.1. MYSQL Connector](#31-mysql-connector)
  - [3.2. Création des packages](#32-cr%C3%A9ation-des-packages)
  - [3.3. Création de la Bean User](#33-cr%C3%A9ation-de-la-bean-user)
    - [3.3.1. Déclaration des propriété de la class User](#331-d%C3%A9claration-des-propri%C3%A9t%C3%A9-de-la-class-user)
    - [3.3.2. Génération du constructeur](#332-g%C3%A9n%C3%A9ration-du-constructeur)
    - [3.3.3. Génération des Getters et Setters](#333-g%C3%A9n%C3%A9ration-des-getters-et-setters)
  - [3.4. DBUtils](#34-dbutils)
  - [3.5. DAO](#35-dao)
    - [3.5.1. Méthodes](#351-m%C3%A9thodes)
    - [3.5.2. getAll()](#352-getall)
    - [3.5.3. getFromRes](#353-getfromres)
    - [3.5.4. Autre méthodes](#354-autre-m%C3%A9thodes)
  - [3.6. Tests unitaires](#36-tests-unitaires)
    - [3.6.1. Fonctionnement des tests unitaires](#361-fonctionnement-des-tests-unitaires)
- [4. Interface: Vue et Controller](#4-interface-vue-et-controller)
  - [4.1. Serveur web](#41-serveur-web)
  - [4.2. Création du projet](#42-cr%C3%A9ation-du-projet)
  - [4.3. Lier au model et driver MySQL](#43-lier-au-model-et-driver-mysql)
  - [4.4. Création de la Servlet (Controller)](#44-cr%C3%A9ation-de-la-servlet-controller)
    - [4.4.1. Process Request](#441-process-request)
    - [4.4.2. Vérifier le bon fonctionnement de notre Servlet](#442-v%C3%A9rifier-le-bon-fonctionnement-de-notre-servlet)
    - [4.4.3. URL MAPPING](#443-url-mapping)
  - [4.5. Forward de Servlet à JSP (vue)](#45-forward-de-servlet-%C3%A0-jsp-vue)
  - [4.6. Index: Affichage des users](#46-index-affichage-des-users)
    - [4.6.1. Affichage du arrayList d'user](#461-affichage-du-arraylist-duser)

# 1. Prequis

- Eclipse IDE for Enterprise Java Developers
- 1 nouveau workspace vide
- MAMP ou WAMP avec phpmyadmin
-

**Petit info que je n'ai précisé à aucun moment dans ce tuto.**
Il peut arriver très souvent que vous ayez des class soulignér en rouge.
Vous avez très certainement oublié de l'importer  
![Erreur d'importation](img/import-error.gif)

# 2. Base de données

Pour cette exercise nous allons créer une base de donnée très simple afin de pouvoir rapidement créer un use case de A-Z sur le modèle MVC2

Connectez vous à phpmyadmin et ajouté une base de donnée que nous appelerons p.ex `easyjee`

ajouter une table `users` contenant 3 colonnes:

- `id`: De type `varchar` en PRIMARY KEY, l'id nous sera utile pour la partie interface, notamment pour l'affichage de l'url (p.ex: localhost/users/edit/{id})
- `username`: de type `varchar` d'une longueur de 255
- `email`: de type `varchar` d'une longueur de 255 avec un index UNIQUE. En définissant l'email comme champs unique cela nous garantit qu'aucun user n'aura le même email

```sql

CREATE TABLE `easyjee`.`users` (
    `id` VARCHAR(40) NOT NULL ,
    `username` VARCHAR(50) NOT NULL ,
    `email` VARCHAR(70) NOT NULL ,
    PRIMARY KEY (`id`),
    UNIQUE (`email`)
) ENGINE = InnoDB;
```

Ajouter déja quelques users à cette base de donnée

```sql
INSERT INTO `users` (`id`, `username`, `email`) VALUES
    ("jo001", 'John', 'john.doe©gmail.com'),
    ("ste002", 'Stephane', 'stephan.king©gmail.com'),
    ("chuck001", 'Chuck', 'chuck.noris©gmail.com'),
    ("robin001", 'Robin', 'hello@robinferrari.ch');
```

# 3. Model

Ajouter un nouveau projet

`New -> Java Project`

Appelez le par example `ModelUser`

## 3.1. MYSQL Connector

Ce modèle aura besoin par la suite de Mysql pour que la DAO puisse communiquer avec la base de donnée.

**Téléchargement**
Télécharger le fichier .jar de mysql connector:
[https://dev.mysql.com/downloads/connector/j/](https://dev.mysql.com/downloads/connector/j/)

Sous "Select Operating System:" choisissez "Platform Independent"

Télécharger le fichier zip décompressez le et copié le fichier .jar dans un dossier `drivers` à la racine de votre workspace

**Intégration au Workspace**
Aller dans les propriétés de votre projet `ModelUser`  
(Bouton droit sur le projet puis propriété ou. CMD+I) puis dans `Java Build path` -> `Librairies` -> `Add External JARs…`
Séléctionner le fichier JAR de mysql précédemment télécharger dans le dossier `drivers` puis cliquez sur "apply and close"

## 3.2. Création des packages

Pour ce projet nous allons avoir besoin de différents package:

`beans` : Contient nos beans, ce sont les classes de base permettant de déclarer nos objets p.ex Class User  
`common`: On retrouvera ici la class DBUtils qui fera le pont entre notre DAO et notre base de donnée  
`dao` : Les DAO sont des class qui permettent aux beans d'effectuer des actions/requête sur la base de donnée p.ex: UserDAO pourra Create Read Update Delete (CRUD) des users dans la base donnée
`test` : Contiendra nos test unitaire pour chacune de nos DAO p.ex UserDAOTest

Créez donc ces package en respectant les convention de nommage, par exemple:

- `com.crea.dev1.jee.model.beans`
- `com.crea.dev1.jee.model.common`
- `com.crea.dev1.jee.model.dao`
- `com.crea.dev1.jee.model.test`

## 3.3. Création de la Bean User

dans le package `com.crea.dev1.jee.model.beans`, ajoutez une User (new -> Class)

### 3.3.1. Déclaration des propriété de la class User

```java
...
	private String id;
	private String username;
	private String email;
...
```

### 3.3.2. Génération du constructeur

Bouton droit dans le fichier puis `source->generate constructor using fields`

Cochez tous les champs et validez

Vos constructeurs devrait ressembler à ca:
_Créez également un constructeur sans argument_

```java
/**
* Constructor
* @param username
* @param email
*/
public User(String username, String email) {
    super();
    this.username = username;
    this.email = email;
    this.id=0;
}
/**
* Init Constructor
*/
public User() {
    this("","");
}
```

### 3.3.3. Génération des Getters et Setters

- Bouton droit dans le fichier puis `source->generate getters and setters`
- Cochez toutes les propriétés

**N'hésitez pas à ajouter des méthodes qui vous semble utiles**

## 3.4. DBUtils

Créer une class `DBUtils` dans le package `com.crea.dev1.jee.model.common`  
Copiez collez le contenu du fichier DBUtils.java  
Adapté les variables `dbPath`, `user` et `password` à votre base de donnée

## 3.5. DAO

Créer une class UserDAO dans le package `com.crea.dev1.jee.model.dao`

### 3.5.1. Méthodes

### 3.5.2. getAll()

Création d'une méthode qui effectura une requête à la base de donnée
Cette méthode retournera un arrayList de tous les users

**Déclaration de la méthode**

```java
public static ArrayList<User> getAll(){
}
```

**Déclaration de la variable `users` qui contiendra tous nos users**

```java
ArrayList<User> users = new ArrayList<User>();
```

**Préparation de la requête SQL et connexion à la base donnée (via DBUtils)**

```java
// On prépare notre requete SQL dans la variable req
String req = "SELECT * FROM users";
// On démarre la connexion au serveur SQL
DBUtils.DBConnexion();
```

**Exécution de la requête**

```java
try {
    DBUtils.setRes(DBUtils.getStm().executeQuery(req));
    while(DBUtils.getRes().next()) {
        ResultSet res = DBUtils.getRes();
    }
    // Stopper la connection
    DBUtils.DBClose();

} catch (SQLException e) {
    e.printStackTrace();
}
```

La class DBUtils retourne une variable de type `ResultSet`qui contiendra
les valeurs retournées par MySQL.  
Cependant nous voulons récupérer nos valeurs dans un objet instancié avec notre class `User`

**Nous assigneront donc nos valeur contenu dans la variable res de la manière suivante:**

```java
...
ResultSet res = DBUtils.getRes();
// Instanciation d'un nouvel objet de type User
User userTemp = new User();
// Assignation des valeur retourné par sql aux la propriétés de notre objet de type User
userTemp.setId(res.getString(1));
userTemp.setUsername(res.getString(2));
userTemp.setEmail(res.getString(3));
users.add(userTemp);
...
```

Nous risquons de devoir effectué cet opération sur chacune de nos méthodes DAO de récupération de données.  
Afin de rester DRY, nous allons créer une fonction `getFromRes` qui effectuera ce travail.

**Notre méthode `getAll` ressemblera donc à ça:**

```java
public static ArrayList<User> getAll(){
    ArrayList<User> users = new ArrayList<User>();
    String req = "SELECT * FROM users";
    DBUtils.DBConnexion();
    try {
        DBUtils.setRes(DBUtils.getStm().executeQuery(req));
        while(DBUtils.getRes().next()) {
            User userTemp = UserDAO.getFromRes(DBUtils.getRes());
            users.add(userTemp);
        }
        DBUtils.DBClose();
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return users;

}
```

### 3.5.3. getFromRes

Voici la méthode getFromRes:
faite attention d'utiliser la méthode adapté au type de votre propriété
res.getInt(NumeroDeColonne) pour un integer, res.getString(NumeroDeColonne) pour une chaine de caractère.  
Voyez tous les types disponible sur [la documentation oracle](https://docs.oracle.com/javase/7/docs/api/java/sql/ResultSet.html)

```java
private static User getFromRes(ResultSet res) throws SQLException {
    User user = new User();
    user.setId(res.getString(1));
    user.setUsername(res.getString(2));
    user.setEmail(res.getString(3));
    return user;
}
```

### 3.5.4. Autre méthodes

Maintenant vous pouvez créer les méthodes suivante:

- `getBy(String column, String value)`: Retourne un arrayList d'objet de type user par la valeur d'un colonne donnée, cette méthode nous permettera de rester DRY en l'utilisant dans note autre méthode de récupération
- `getOneBy(String column, String value)` : Utiliser getBy pour retourner 1 seul objet de type User
- `getById(String id)`: Retourne un objet de type User par son id
- `getByEmail(String email)`: Retourne un objet de type User par son email
- `add(User user)`: Ajout d'un user à la base de donnée
- `update(User user)`: Mise à jour d'un user dans la base de donnée
- `delete(String id)`: Suppression d'un user de la base donnée

Si vous bloquez regardez le fichier `UserDAO.java` dans ce repository

**N'hésitez pas à ajouter des méthodes qui vous semble utiles**

## 3.6. Tests unitaires

- Dans le package `com.crea.dev1.jee.model.test`, créez un nouvelle class de Test JUnit (`UserDAOTest`)
- Bouton droit sur la package puis: `new->JUnit test case`
  - Si vous ne trouver pas `JUnit test case` cliquez sur `Other` et recherchez `JUnit test case`
- Eclipse vous demandera à ce moment d'ajouter la librairie JUnit 5 au build path: acceptez et cliquez sur OK

### 3.6.1. Fonctionnement des tests unitaires

Nous utilison JUnit pour nos test unitaire, pour plus d'info veuillez consulter [La documentation](http://junit.sourceforge.net/javadoc/org/junit/package-summary.html)

Afin d'éviter de devoir regénérer notre base de donnée à chaques tests, et ne sachant pas forcément quel éléments se trouve dans notre base de donnée nous allons utiliser les fonction @BeforeEach et @AfterEach de JUnit pou ajouter et supprimer des utilisateur de référence. @BeforeEach s'execute avant chaque @Test et @AfterEach après chaque @Test.

- `@BeforeEach`: Ici nous insérerons dans le base de données différents user que nous utiliserons comme références pour tous nos test unitaires
- `@AfterEach`: Ici nous supprimerons les users de référence créer à l'étape précédente

Pour les utilisé il faut importer leur librairie en haut du fichier de test:

```java
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
```

En propriété de notre class de test nous allons définire nos User de référence:

```java
User ref1 = new User("TAHA001","Taha Ridene", "mvc@troistiers.ch");
User ref2 = new User("ROBIN002","Robin Ferrari", "no-reply@robinferrari.ch");
```

Nous aurions pu utiliser des UUID pour nos id, voici comment générer un UUID en Java:

```java
String uuid = UUID.randomUUID().toString();
```

Déclaration de @BeforeEach et @AfterEach  
_Notez que l'on utilise également les asserts dans ces méthodes_

```java
@BeforeEach
public void testAdd() {
    // Ajouter les deux références et vérifier si la DAO retourne "true"
    assertTrue(UserDAO.add(this.ref1));
    assertTrue(UserDAO.add(this.ref2));

    // Ajout d'un utilisateur existant dans la base de donnée
    // Pour vérifier que celui-ci est bien refusé par Mysql
    assertFalse(UserDAO.add(this.ref2));

}
@AfterEach
public void testDelete() {
    // Supprimer les deux références et vérifier si la DAO retourne "true"
    assertTrue(UserDAO.delete(ref1.getId()));
    assertTrue(UserDAO.delete(ref2.getId()));
    // Suppression d'un utilisateur non-existant dans la base de donnée
    // Pour vérifier que MYSQL retourne bien false
    assertFalse(UserDAO.delete(ref1.getId()));
}
```

# 4. Interface: Vue et Controller

## 4.1. Serveur web

Pour afficher notre interface nous allons avoir besoin d'un server  
Nous utiliserons ici Tomcat v9

**Avant de créer le projet**

- télécharger Tomcat V9 en cliquant sur ce [ICI](https://tomcat.apache.org/download-90.cgi)
- Dezipper le et copier le dossier dans un dossier `servers` à la racine de votre Workspace

## 4.2. Création du projet

Ajouter un nouveau projet web dynamique à votre Workspace `file->new->Dynamic Web Project`

_(Si vous ne trouvez pas Dynamic Web Project cliquez sur `file->new->other` puis recherchez Dynamic Web Project)_

- Nommez votre Projet par example IntefaceEasyJEE
- Dans Target runtime cliquer sur `new target run time`
- Séléctionnez Apache Tomcat v9.0
- Cliquez sur next
- Dans tomcat Installation directory cliquez sur browse
- Séléctionner le dossier de tomcat précdement copié dans le dosssier server à la racine de votre workspace
- dans JRE séléctionner Java SE 8
- Cliquez sur Finish
- Cliquez sur next 2x
- Cochez `generate web.xml deployment descriptor`
- Cliquez sur Finish

_A ce stade vous aurez peut être le message suivant: `This kind of project is associated with the Java EE perspective. Do you want to open this perspective now?:`_ **Cliquez sur open perspective**

## 4.3. Lier au model et driver MySQL

Notre interface étant dans un projet séparé du model nous allons devoir les lier l'un à l'autre:

- Bouton droit sur InterfaceEasyJEE -> propriété -> Java Build PATH -> Projects
- Cochez ModelEasyJEE et cliquer sur Apply

Notre interface utilisera aussi MySQL à travers notre DAO, il faut donc ajouter le driver mysql:

- bouton droit sur le projet -> properties -> Java Build Path -> Librairies -> Add External JARs
- Séléctionner le fichier .jar de mysql dans le dossier `drivers` à la racine de votre workspace
- Cliquez sur Apply and Close

Eclipse aura donc accès à cette librairie,
mais ne l'utilisera pas lors du déploiement
tout commme il n'utilisera pas notre projet ModelEasyJee

Nous allons donc définir les ressource requise pour le déployement:

- InterfaceEasyJEE -> properties -> Web Deployment Assembly -> add... -> java Build Path Entries -> mysql-connector…
- Cliquez sur finish mais garder la fenêtre properties ouverte
- add... -> Projects -> ModelEasyJEE
- Cliquez sur finish
- Cliquez sur Apply and close

## 4.4. Création de la Servlet (Controller)

Notre servlet sera le controller qui s'occupera de traiter les différentes requêtes, de rediriger vers les bon JSP Avec les bonnes données, Ce controller utilisera la DAO et les beans créer précédemment dans la partie Model.

- Dans Java Resource -> src créez une nouveau package `com.crea.dev1.jee.interfac.controller`
- Ajouter une nouvelle Servlet `UserController` dans ce package (btn droit sur le package -> new -> Servlet)

### 4.4.1. Process Request

Pour cette exercice nous n'allons pas différencier les requètes GET et POST.  
Nous allons donc créer une fonction processRequest qui sera appelé par doGet et doPost.

**FONCTIONNEMENT DE PROCESS REQUESTE**

Process request prendra en entrée un nom d'action et devra effectué les bonnes opérations en fonction de l'action demandée:

Nous aurons deux types d'actions:

- Les action d'affichages: qui redirigerons vers la bonne vue
- Les actions DAO qui communiquerons et effectuerons des actions sur la base de donnée

**Actions d'affichage**

Nous récupérerons la variable action en fonction de notre url

- `/users`: Affiche la liste des users
- `/users/add`: Affiche un formulaire d'ajout d'user
- `/edit/{userId}`: Affiche un formulaire d'un user donné
- `/show/{userId}`: Affiche le détail d'un utilisateur donnée

**Action DAO**

Nous récupérerons la variable action dans la requete post, cette variable sera transmise par les formulaires qui ferons appel à ces actions.

- `create`: Créer un nouvel user dans la base de donnée
- `update`: Procède a la mise à jour d'un user en base de donnée
- `remove`: Supprime un utilisateur donné

```java
protected void processRequest(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException, SQLException {
        // ICI VIENDRA LE TRAITEMENT DE NOS REQUETES
}

protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    try {
        processRequest(request, response);
    } catch (SQLException e) {
        e.printStackTrace();
    }
}
protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    doGet(request, response);
}
```

### 4.4.2. Vérifier le bon fonctionnement de notre Servlet

Le point d'entrée de nos user sera: {baseUrl}/users  
Nous allons donc configurer notre projet pour que cette url envoie les requetes à notre Servlet `UserController`

- Ouvrez web.xml (InterfaceEasyJEE/WebContent/WEB-INF/web.xml)
- Déclarez votre servlet en indiquant son nom et son package
- Définissez l'url pattern correspondant à votre Servlet (pour que les requête des ces urls passe par votre Servlet)
- Configurez votre Servlet et son mapping comme suit:

```xml
<servlet>
    <servlet-name>UserController</servlet-name>
    <servlet-class>com.crea.dev1.jee.interfac.controller.UserController</servlet-class>
</servlet>
<servlet-mapping>
    <servlet-name>UserController</servlet-name>
    <url-pattern>/users/*</url-pattern>
</servlet-mapping>
```

- Ajouter un message à l'aide de System.out.println dans votre Servlet
- Runnez votre Serveur et accéder à l'url corresponde (ici: http://localhost:8080/InterfaceEasyJEE/users)
- Votre console devrait afficher le message que vous venez d'ajouter à votre Servlet, si ce n'est pas le cas vous avez du raté une étape

### 4.4.3. URL MAPPING

**Récupérer l'action et sa valeur associée depuis l'url**

Nous devons maintenant créer une méthode qui nous permettera de récupérer les info de l'url
par example pour /users/edit/AGUE001 nous allons vouloir récupérer deux variables:

- `action`: edit
- `actionValue`: AGUE001

Notre nouvelle méthode prendra donc la requete en paramètre et retourera un tableau contenant ces deux variables

```java
/**
    * Méthode permettant de récupérer l'action et la valeur associé à cette action de l'url.

    * Par exemple: Pour http://localhost:8080/InterfaceEasyJEE/users/edit/AGUE001
    * nous obtiendront un tableau avec à l'index 0 "edit" et a l'index 1 "AGUE001"
    *
    * @param request
    * @return String[]: [0]: action [1]: actionValue
    */
protected String[] getActionRequest(HttpServletRequest request) {

    // Définition de l'action par défault et des autre variables
    String action = "index";
    String actionValue = null;
    String[] returnValue = new String[2];

    // Récupération de l'url du projet
    // request.getRequestURI() retournera l'url complete p.ex(localhost:8080/InterfaceEasyJEE/user/add)
    // request.getContextPath() retournera l'url du point d'entrée (p.ex: localhost:8080/InterfaceEasyJEE/)
    // Nous allons supprimer l'url du point d'entrée de notre url complete
    // Nous aurons ainsi uniquement le "path"
    // Le path est la partie de l'url qui nous intéresse (p.ex: users/add/ )
    String path = request.getRequestURI().substring(request.getContextPath().length() + 1);
    System.out.println("Path: " + path);

    // Convertion du path (p.ex /users/edit/AGUE001) en tableau de string
    String[] pathParts = path.split("/");
    Integer nbPathParts = pathParts.length;

    System.out.println("pathParts: " + Arrays.toString(pathParts));
    System.out.println("nbPathParts: " + nbPathParts);


    if(nbPathParts == 2) {
        //p.ex:  /users/add (/1/2)
        action = pathParts[1];
    }
    if(nbPathParts == 3 ) {
        // /users/edit/AGUE001
        // /1/2/3
        action = pathParts[1];
        actionValue = pathParts[2];
    }

    returnValue[0] = action;
    returnValue[1] = actionValue;

    System.out.println("Action: " + action);
    System.out.println("actionValue: " + actionValue);

    return returnValue;
}
```

Nous allons maintenant tester que notre méthode fonctionne en l'appelant dans la fonction processRequest créer précédemment:

```java
String[] actionRequest = this.getActionRequest(request);
System.out.println("Action: " + actionRequest[0]);
System.out.println("ActionValue: " + actionRequest[1]);
```

- Accéder maintenant à l'url suivante: [http://localhost:8080/InterfaceEasyJEE/users/edit/AGUE001](http://localhost:8080/InterfaceEasyJEE/users/edit/AGUE001)
- Votre console devrai vous afficher:

```
Action: edit
ActionValue: AGUE001
```

## 4.5. Forward de Servlet à JSP (vue)

Nous allons tous t'abord créer des fichiers JSP pour pouvoir tester nos redirectons

- Créer un dossier `views` dans `InterfaceEasyJEE/WebContent/`
- Créer un dossier `users` dans `InterfaceEasyJEE/WebContent/views`
- Créer un fichier jsp `index.jsp` dans le dossier `views` (new->JSP file...)
- Ajouter un message sur ce fichier du style: "Hello index"
- Fait la même chose pour `add.jsp`, `show.jsp`, `edit.jsp`

Maintenant nous allons modifier notre controlleur(Servlet) pour qu'il nous redirige
vers la bonne vue en fonction de l'action demandée (dans la méthode processRequest):

```java
...
// Récupération de le l'action et sa valeur associé via l'url
String[] actionRequest = this.getActionRequest(request);
String action = actionRequest[0];
String actionValue = actionRequest[1];

// Initialisation du forward
String forward = null;

// Répartition des actions
switch(action) {
    case("index"):
        forward = "views/users/index.jsp";
        break;
    case("show"):
        forward = "views/users/show.jsp";
        break;
    case("add"):
        forward = "views/users/add.jsp";
        break;
    case("edit"):
        forward = "views/users/edit.jsp";
        break;
    default:
        forward = "404.jsp";
        break;
}

// Affichage de la jsp correspondante à l'action demandée avec RequestDispatcher
RequestDispatcher dispatcher = request.getRequestDispatcher("/"+forward);
dispatcher.forward(request, response);
...
```

Tester maintenant les différentes urls afin d'être sûre que votre Servlet affiche la bonne vue

- [http://localhost:8080/InterfaceEasyJEE/users/](http://localhost:8080/InterfaceEasyJEE/users/)
- [http://localhost:8080/InterfaceEasyJEE/users/](http://localhost:8080/InterfaceEasyJEE/users/add)
- [http://localhost:8080/InterfaceEasyJEE/users/](http://localhost:8080/InterfaceEasyJEE/users/edit/AGUE001)
- [http://localhost:8080/InterfaceEasyJEE/users/](http://localhost:8080/InterfaceEasyJEE/users/show/AGUE001)

## 4.6. Index: Affichage des users

Pour que note index.jsp puisse afficher la liste des users,
nous allons devoir lui passer en paramètre un arrayList d'users

Nous utiliserons donc notre DAO:

```java
...
case("index"):
    ArrayList<User> users = UserDAO.getAll();
    request.setAttribute("users", users);
    forward = "views/users/index.jsp";
    break;
...
```

- Sauvegarder et relancer votre serveur
- Vérifier que rien ne bug si vous accédez à :
- Si une erreur 500 apparrait vous avez très certainement rater l'étapte (Lier au model et driver MySQL)

### 4.6.1. Affichage du arrayList d'user

Tout d'abord nous allons devoir importer les classes nécessaire à notre page:

- ArrayList : Pour pouvoir récuperer notre liste d'user
- User: Pour utiliser la Bean User

```java
<%@page import="java.util.ArrayList"%>
<%@page import="com.crea.dev1.jee.model.beans.User"%>
```

Nous pouvons à présent récupérer notre liste d'user via les attributs de la requête:

```jsp
<% ArrayList<User> users = (ArrayList<User>) request.getAttribute("users"); %>
```

A partir de là nous pouvons faire une boucle sur la variable user et afficher nos users:

```jsp
<a href="add">Ajouter un utilisateur</a>
<ul>
<% for(User user:users){ %>
    <li>
        <%=user.getId() %> -
        <%=user.getUsername() %> -
        <%=user.getEmail() %> |
        <a href="show/<%=user.getId() %>">Afficher</a> -
        <a href="edit/<%=user.getId() %>">Modifier</a>
    </li>
<% } %>
</ul>
```
