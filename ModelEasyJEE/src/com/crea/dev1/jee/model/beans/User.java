package com.crea.dev1.jee.model.beans;

public class User {
	private String id;
	private String username;
	private String email;
	
	/**
	 * Constructor
	 * @param username
	 * @param email
	 */
	public User(String id, String username, String email) {
		super();
		this.username = username;
		this.email = email;
		this.id=id;
	}
	/**
	 * Init Constructor
	 */
	public User() {
		this("","","");
	}
	
	/**
	 * Getters and Setters
	 */
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
}
