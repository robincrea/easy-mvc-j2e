package com.crea.dev1.jee.model.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.crea.dev1.jee.model.beans.User;
import com.crea.dev1.jee.model.common.DBUtils;

public class UserDAO {

	/**
	 * retourne tous les user dans un arraylist
	 * @return ArrayList<User> : Liste des users
	 * @throws SQLException
	 */
	public static ArrayList<User> getAll(){
		
	    ArrayList<User> users = new ArrayList<User>();

	    // On prépare notre requete SQL dans la variable req
		String req = "SELECT * FROM users";
	
		// On démarre la connexion au serveur SQL
		DBUtils.DBConnexion();
		
		// Execution de la requete 
		try {
			DBUtils.setRes(DBUtils.getStm().executeQuery(req));
			while(DBUtils.getRes().next()) {
				User userTemp = UserDAO.getFromRes(DBUtils.getRes());
				users.add(userTemp);
			}
			// Stopper la connection
			DBUtils.DBClose();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return users;
								
	}
	
	
	public static User getById(String id){
		// Comme notre méthode getBy ne prend que des String (Car ensuite requete SQL type String)
		// Nous allons convertir notre id de type int en type String
		return UserDAO.getOneBy("id", id);
	}
	
	public static User getByEmail(String email){
		return UserDAO.getOneBy("email", email);
	}
	public static User getByUsername(String username){
		return UserDAO.getOneBy("username", username);
	}
	
	public static boolean add(User user) {
	
		// On prépare les valeur qui pour notre requête sql et les enregistrer dans une variable
		// Le contenu de la variable devrai ressemble à qqch comme ca: 
		// ('2398u432sdfs','robin', 'robin@octree.ch') <-- les single quote sont important
		String userValues = "'"+user.getId()+"','"+user.getUsername()+ "','"+user.getEmail()+"'";
		
		// On prépare notre requete SQL
		String req = "INSERT INTO `users` (`id`, `username`, `email`) VALUES ("+userValues+")";
		
		
		try {
			// DEMMARER LA CONNECTION
			DBUtils.DBConnexion();
			DBUtils.getStm().executeUpdate(req) ;
			System.out.println("L'user: " + user.getUsername() + " a bien été ajouté");
			return true;
	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("L'user: " + user.getUsername() + " n'a pas pu être ajouté");
			// e.printStackTrace();
			return false;
		}
		
	}
	
	/**
	 * Mise à jour d'un user par son email
	 * @param user
	 * @return 
	 * @throws SQLException
	 */
	
	private static int update(String id, String column, String value) throws SQLException {
		
		int result = -1;
		DBUtils.DBConnexion();
		String req = "UPDATE users SET "+column+" = '"+value+"' WHERE id='"+id+"' ";
		result = DBUtils.getStm().executeUpdate(req);
		System.out.println("Requete executée");
		DBUtils.DBClose();
		return result;
	}
	
	
	
	public static int updateUsername(String id, String username) {
		try {
			return UserDAO.update(id, "username", username);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
	}
	
	
	/**
	 * Suppression d'un user de la base de donnée par son id
	 * @param id string
	 * @return boolean
	 */
	public static boolean delete(String id) {		
		try {
			// On prépare notre requete SQL
			String req = "DELETE FROM users WHERE users.id = '"+id+"' ";
		
			// DEMMARER LA CONNECTION
			DBUtils.DBConnexion();
			
			// Execution de la requete et init
			if(DBUtils.getStm().executeUpdate(req) > 0) {
				System.out.println("L'user avec id: " + id +" a bien été supprimé");
				return true;
			}else{
				System.out.println("L'user avec l'id: " + id +" n'existe pas ou ne peut pas être supprimé");
				return false;
			}
		}catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
	}
	
	/**
	 * Récupère un arraylist contenant les livre d'une colonne donnée correspondant à
	 * une valeur donné
	 * 
	 * @param column : Nom de la colonne dans la bdd
	 * @param value
	 * @return 
	 */
	private static ArrayList<User> getBy(String column, String value) {
		
		// Initialisation d'une variable users qui contiendera les résultat de notre requête
		ArrayList<User> users = new ArrayList<User>();

		// On prépare notre requete SQL
		String req = "SELECT * FROM users WHERE users." + column + "='" + value + "'";

		try {
			// DEMMARER LA CONNECTION
			DBUtils.DBConnexion();
			// Exécution de la requête
			DBUtils.setRes(DBUtils.getStm().executeQuery(req));
			
			// Boucle sur chacun des résultat de la requette
			while (DBUtils.getRes().next()) {
				// Assignation dea valeur retourné par un des résultat dans un objet de type User
				User elevTemp = UserDAO.getFromRes(DBUtils.getRes());
				
				// Ajout de l'user dans le tableau d'user
				users.add(elevTemp);
			}

			// Stopper la connection
			DBUtils.DBClose();
			
		} catch (SQLException e) {
			// Afficage de la trace en cas d'erreur
			e.printStackTrace();
		}
		
		// retour des résultat 
		return users;
	}
	
	public static User getOneBy(String column, String value) {
		// Exécute la méthode getBy et retourne uniquement le 1er résultat
		return UserDAO.getBy(column, value).get(0);

	}
	
	
	private static User getFromRes(ResultSet res) throws SQLException {
		User user = new User();
		user.setId(res.getString(1));
		user.setUsername(res.getString(2));
		user.setEmail(res.getString(3));
		return user;
	}

	

}
