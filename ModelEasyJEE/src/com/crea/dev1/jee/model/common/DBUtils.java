package com.crea.dev1.jee.model.common;

/**
 * 
 * @author Taha RIDENE
 */
//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DBUtils {
  
  private static final String dbPath   = "jdbc:mysql://localhost:8889/easyjee?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC"; //No de port mysql 3306, 8889 sous mac
  private static final String user     = "root";
  private static final String password = "root";
  private static Connection   con      = null;
  private static Statement    stm      = null;
  private static ResultSet    res      = null;
  private static int          erreur;

  public static Exception DBConnexion() {
      try {
          Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
          con = DriverManager.getConnection(dbPath, user, password);
          stm = con.createStatement();
          return null;
      } catch (Exception e) {
          System.out.println("ERROR in Connexion to " + dbPath + " :" + e.getMessage());
          return e;
      }
  }

  public static int DBClose() 
  {
      try {
          stm.close();
          con.close();
      } catch (Exception e) {
          System.out.println("ERROR in Connexion closure to " + dbPath + " : " + e.getMessage());
      }

      return erreur;
  }

  public static Connection getCon() {
      return con;
  }

  public static void setCon(Connection con) {
      DBUtils.con = con;
  }

  public static Statement getStm() {
      return stm;
  }

  public static void setStm(Statement stm) {
      DBUtils.stm = stm;
  }

  public static ResultSet getRes() {
      return res;
  }

  public static void setRes(ResultSet res) {
      DBUtils.res = res;
  }
	
  public static void initDB() {
	  // TODO Auto-generated method stub
	  
  }
}


//~ Formatted by Jindent --- http://www.jindent.com
