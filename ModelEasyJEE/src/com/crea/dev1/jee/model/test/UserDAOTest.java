package com.crea.dev1.jee.model.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;


import com.crea.dev1.jee.model.beans.User;
import com.crea.dev1.jee.model.dao.UserDAO;

class UserDAOTest {
		
	User ref1 = new User(UUID.randomUUID().toString(),"Taha Ridene", "mvc@troistiers.ch");
	User ref2 = new User(UUID.randomUUID().toString(),"Robin Ferrari", "no-reply@robinferrari.ch");
	
	@BeforeEach
	public void testAdd() {
		// Ajouter les deux références et vérifier si la DAO retourne "true"
		assertTrue(UserDAO.add(this.ref1));
		assertTrue(UserDAO.add(this.ref2));
		
		// Ajout d'un utilisateur existant dans la base de donnée
		// Pour vérifier que celui-ci est bien refusé par Mysql
		assertFalse(UserDAO.add(this.ref2));

    }
	@AfterEach
    public void testDelete() {
		// Supprimer les deux références et vérifier si la DAO retourne "true"
		assertTrue(UserDAO.delete(ref1.getId()));
		assertTrue(UserDAO.delete(ref2.getId()));
		// Suppression d'un utilisateur non-existant dans la base de donnée
		// Pour vérifier que MYSQL retourne bien false
		assertFalse(UserDAO.delete(ref1.getId()));
    }
	
	@Test
	void testAddAndDelete() {
		// ICI on a pas besoin de faire de test 
		// vu qu'ils sont executé dans le After et Before each
		// Si il ne passe pas aucun autre test ne passera
		assertTrue(true);
	}
	
	@Test
	void testGetByUsername(){
		User userget = UserDAO.getByUsername(ref1.getUsername());
		assertEquals(ref1.getUsername(), userget.getUsername());
	}
	
	@Test
	void testGetByEmail(){
		User userget = UserDAO.getByEmail(ref1.getEmail());
		assertEquals(ref1.getEmail(), userget.getEmail());
		
		userget = UserDAO.getByEmail(ref2.getEmail());
		assertEquals(ref2.getEmail(), userget.getEmail());
	}
	@Test
	void testGetById(){

		User userget = UserDAO.getById(ref1.getId());
		assertEquals(userget.getEmail(), ref1.getEmail());
		assertEquals(userget.getUsername(), ref1.getUsername());
		assertEquals(userget.getId(), ref1.getId());
		
	}
	
	@Test
	void testUpdate(){
		assertTrue( UserDAO.updateUsername(ref1.getId(), "New Username") > 0 );
		User updatedUser = UserDAO.getById(ref1.getId());		
		assertEquals(updatedUser.getUsername(), "New Username");	
	}
	
	
	

}
